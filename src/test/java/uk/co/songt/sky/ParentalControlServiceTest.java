package uk.co.songt.sky;

import com.google.inject.Inject;
import org.jukito.JukitoModule;
import org.jukito.JukitoRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import uk.co.songt.sky.exception.ParentalControlException;
import uk.co.songt.sky.guice.module.ApplicationModule;
import uk.co.songt.sky.TestConstant;

@RunWith(JukitoRunner.class)
public class ParentalControlServiceTest {

    public static class TestModule extends JukitoModule {
        @Override
        protected void configureTest() {
            install(new ApplicationModule());
        }
    }

    @Inject
    ParentalControlService parentalControlService;

    @Test
    public void testCanWatch(){
        assert  parentalControlService.canWatch(ParentalControlLevel.U,TestConstant.MOVIEIDU) ==true;
    }

    @Test(expected=ParentalControlException.class)
    public void testCanWatchTitleNotFound(){
        parentalControlService.canWatch(ParentalControlLevel.U,TestConstant.MOVIEID);
    }

}