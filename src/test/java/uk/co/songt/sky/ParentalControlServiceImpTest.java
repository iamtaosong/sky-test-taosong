package uk.co.songt.sky;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import uk.co.songt.sky.exception.ParentalControlException;
import uk.co.songt.sky.exception.TechnicalFailureException;
import uk.co.songt.sky.exception.TitleNotFoundException;
import uk.co.songt.sky.imp.ParentalControlServiceImp;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

//@RunWith(JukitoRunner.class)
@RunWith(MockitoJUnitRunner.class)
public class ParentalControlServiceImpTest {


    @InjectMocks
    protected ParentalControlServiceImp parentalControlService;
    @Mock
    MovieService movieServiceImp;
    @Mock
    private com.google.inject.Provider<MovieService> movieServiceProvider;


    @Before
    public void setup(){
    }
    @Test
    public void testCanWatch(){
        when(movieServiceImp.getParentalControlLevel(any())).thenReturn(ParentalControlLevel.TWELVE.getName());
        assert  parentalControlService.canWatch(ParentalControlLevel.FIFTEEN,TestConstant.MOVIEID)==true;
    }

    @Test(expected=ParentalControlException.class)
    public void testCanWatchNUll(){
        when(movieServiceImp.getParentalControlLevel(any())).thenReturn(null);
        assert  parentalControlService.canWatch(ParentalControlLevel.FIFTEEN,any())==true;
    }

    @Test(expected=ParentalControlException.class)
    public void testCanWatchTileNotFound(){
        when(movieServiceImp.getParentalControlLevel(any())).thenThrow(new TitleNotFoundException());
        assert  parentalControlService.canWatch(ParentalControlLevel.FIFTEEN,any())==true;
    }

    @Test
    public void testCanWatchTechnical(){
        when(movieServiceImp.getParentalControlLevel(any())).thenThrow(new TechnicalFailureException());
       assert  parentalControlService.canWatch(ParentalControlLevel.FIFTEEN,any())==false ;
    }
}