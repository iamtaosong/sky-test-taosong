package uk.co.songt.sky;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.songt.sky.guice.module.ApplicationModule;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);
    public static void main(String[] args){
        Injector injector = Guice.createInjector(new ApplicationModule());
        ParentalControlService parentalControlServicearentalControlService = injector.getInstance(ParentalControlService.class);
        boolean allowed = parentalControlServicearentalControlService.canWatch(ParentalControlLevel.EIGHTEEN,"movieId15");
        LOG.info("A customer with prefered parent control level "+ ParentalControlLevel.EIGHTEEN.getName() + " for movie id movieId15 is " + ((allowed==true)?"allowed":"not allowd"));
    }
}
