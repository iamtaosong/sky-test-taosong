package uk.co.songt.sky.guice.module;


import com.google.inject.AbstractModule;

public class ApplicationModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new ServiceModule());
    }

}
