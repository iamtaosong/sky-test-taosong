package uk.co.songt.sky.guice.module;


import com.google.inject.Binder;
import com.google.inject.Module;
import uk.co.songt.sky.MovieService;
import uk.co.songt.sky.ParentalControlService;
import uk.co.songt.sky.imp.MovieServiceSimulatorImp;
import uk.co.songt.sky.imp.ParentalControlServiceImp;

public class ServiceModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(MovieService.class).to(MovieServiceSimulatorImp.class).asEagerSingleton();
        binder.bind(ParentalControlService.class).to(ParentalControlServiceImp.class).asEagerSingleton();
    }
}
