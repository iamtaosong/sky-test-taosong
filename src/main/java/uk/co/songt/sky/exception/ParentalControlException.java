package uk.co.songt.sky.exception;


public class ParentalControlException extends RuntimeException {

    String message;
    public ParentalControlException(String message) {
        super(message);
    }
}
