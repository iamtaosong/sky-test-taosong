package uk.co.songt.sky;


import uk.co.songt.sky.exception.TechnicalFailureException;
import uk.co.songt.sky.exception.TitleNotFoundException;

public interface MovieService {
    String getParentalControlLevel(String movieId) throws TitleNotFoundException, TechnicalFailureException;
}
