package uk.co.songt.sky.util;

import uk.co.songt.sky.ParentalControlLevel;

import java.util.Optional;
import java.util.stream.Stream;

public class ParentalSeviceUtil {

    public static Optional<ParentalControlLevel> getParentalControlLevelFromString(String controlLevel)  {
       return  Stream.of(ParentalControlLevel.values()).filter(level->level.getName().equalsIgnoreCase(controlLevel))
                .findFirst();
    }

    public static Optional<ParentalControlLevel> get(String controlLevel)  {
        return  Stream.of(ParentalControlLevel.values()).filter(level->level.getName().equalsIgnoreCase(controlLevel))
                .findFirst();
    }

}
