package uk.co.songt.sky;

public interface ParentalControlService {
    public boolean canWatch(ParentalControlLevel contrlLevel,String moveiId );
}
