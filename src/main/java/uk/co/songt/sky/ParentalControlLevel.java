package uk.co.songt.sky;

public enum ParentalControlLevel {
    U(1,"U"),
    PG(2,"PG"),
    TWELVE(3,"12"),
    FIFTEEN(4,"15"),
    EIGHTEEN(5,"18");

    private String name;
    private int level;
    private ParentalControlLevel(int level, String name){
        this.level=level;
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public int getLevel() {
        return level;
    }

}
