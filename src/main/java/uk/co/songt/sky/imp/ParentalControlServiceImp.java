package uk.co.songt.sky.imp;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.songt.sky.MovieService;
import uk.co.songt.sky.ParentalControlLevel;
import uk.co.songt.sky.ParentalControlService;
import uk.co.songt.sky.exception.ParentalControlException;
import uk.co.songt.sky.exception.TechnicalFailureException;
import uk.co.songt.sky.exception.TitleNotFoundException;
import uk.co.songt.sky.util.ParentalSeviceUtil;


import java.util.Optional;

public class ParentalControlServiceImp implements ParentalControlService {

    private static final Logger LOG = LoggerFactory.getLogger(ParentalControlServiceImp.class);
    private final MovieService movieService;
    @Inject
    protected ParentalControlServiceImp( MovieService movieService ) {
        super();
        this.movieService = movieService;
    }

    public ParentalControlLevel getParentalControlLevel(String controlLevel) throws ParentalControlException{

        Optional<ParentalControlLevel> parentalLevel = ParentalSeviceUtil.getParentalControlLevelFromString(controlLevel);
        if(parentalLevel.isPresent()){
           return  parentalLevel.get();
        }
        else{
            LOG.error("Can't get proper parental control from movice service");
            throw new ParentalControlException("Can't get proper parental control from movice service");
        }
    }


    public boolean canWatch(ParentalControlLevel preferedContrlLevel, String movieId ){
        try {
            String controlLevel = movieService.getParentalControlLevel(movieId);
            ParentalControlLevel responseLevel =   getParentalControlLevel(controlLevel);
            return (responseLevel.getLevel()<=preferedContrlLevel.getLevel());
        }
        catch(TitleNotFoundException ex){
            LOG.error("Caught Exception", ex);
            throw new ParentalControlException("Moive tile can't be found");
        }
        catch(TechnicalFailureException ex){
            LOG.error("Caught Exception", ex);
            return false;
        }
    }
}
