package uk.co.songt.sky.imp;

import uk.co.songt.sky.MovieService;
import uk.co.songt.sky.ParentalControlLevel;
import uk.co.songt.sky.exception.TechnicalFailureException;
import uk.co.songt.sky.exception.TitleNotFoundException;

public class MovieServiceSimulatorImp implements MovieService{

    @Override
    public String getParentalControlLevel(String movieId) throws TitleNotFoundException, TechnicalFailureException{

        switch(movieId){
            case "movieIdU" :return ParentalControlLevel.U.getName();
            case "movieIdPG" :return ParentalControlLevel.PG.getName();
            case "movieId12" :return ParentalControlLevel.TWELVE.getName();
            case "movieId15" :return ParentalControlLevel.FIFTEEN.getName();
            case "movieId18" :return ParentalControlLevel.EIGHTEEN.getName();
            default : throw new TitleNotFoundException();
        }
    }
}