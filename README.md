## This app is written in Java 8, built in gradle. It uses google juice for dependency injection.
##As the Sky test requirement only lists the Parental Control Levels(U, PG, 12, 15, 18) without indicating the order from high to low.
##This test application implements a MovieService Simulator class and ParentalControlLevel enum class just to demonstrate the solution

## Test application, it will show test results on terminal, if you like to see the test result UI, please see <project dir>/build/reports/tests/index.html 

./testApp.sh

## Run application

./runApp.sh

## Build application
 ./buildApp.sh
 
## Run  jar file
cd <prject dir>/build/libs 
java -jar sky-test-taosong-all-1.0-SNAPSHOT.jar 